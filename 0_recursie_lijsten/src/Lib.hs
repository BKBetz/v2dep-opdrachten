module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- Schrijf een functie die de som van een lijst getallen berekent, net als de `product` functie uit de les.
-- | Maak een loop dat elke keer de tail van de lijst opnieuw door de functie laat lopen en zo alles bij elkaar optelt
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 list = head list + ex1 (tail list)

-- Schrijf een functie die alle elementen van een lijst met 1 ophoogt; bijvoorbeeld [1,2,3] -> [2,3,4]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- | Tel bij elke head van een lijst een op en verstuur de tail weer opnieuw door de functie heen
ex2 :: [Int] -> [Int]
ex2 [] = []
ex2 (x:xs) = x + 1:ex2 xs

-- Schrijf een functie die alle elementen van een lijst met -1 vermenigvuldigt; bijvoorbeeld [1,-2,3] -> [-1,2,-3]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- | Pak de eerste item van x, vermenigvuldig deze met 1 en roep de functie vervolgens opnieuw met de tail van x
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 (x:xs) = x * (-1):ex3 xs

-- Schrijf een functie die twee lijsten aan elkaar plakt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1,2,3,4,5,6]. Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.
-- | Voeg elke keer de eerste item van x toe aan y en roep ex4 weer met de tail van x en y
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] y = y
ex4 (x:xs) y= x:ex4 xs y

-- Schrijf een functie die twee lijsten paarsgewijs bij elkaar optelt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1+4, 2+5, 3+6] oftewel [5,7,9]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- | Door gebruik te maken van (x:xs) en (y:ys) kan je telkens de 2 heads bij elkaar optellen en dan de 2 tails weer doorsturen
ex5 :: [Int] -> [Int] -> [Int]
ex5 _[] = []
ex5 []_ =[]
ex5 (x:xs) (y:ys) = x + y:ex5 xs ys

-- Schrijf een functie die twee lijsten paarsgewijs met elkaar vermenigvuldigt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1*4, 2*5, 3*6] oftewel [4,10,18]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- | gebruik (x:xs) en (y:ys) vermenigvuldig x met y en stuur xs en ys weer door
ex6 :: [Int] -> [Int] -> [Int]
ex6 _[] = []
ex6 []_ = []
ex6 (x:xs) (y:ys) = x * y :ex6 xs ys

-- Schrijf een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig prodct uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
-- | Ik heb hier letterlijk gedaan wat de opdracht zei ik heb 1 en 6 gecombineerd door twee heads met elkaar te vermenigvuldigen en dat steeds op tellen met de eerstevolgende getallen door de tails weer door te sturen
ex7 :: [Int] -> [Int] -> Int
ex7 [][] = 0
ex7 (x:xs) (y:ys) = x * y + ex7 xs ys
